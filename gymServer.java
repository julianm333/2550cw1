
import java.sql.*;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.net.*;

public class gymServer {
    public static void main(String args[]) {
	try {
	    final int port = 4000;
	    ServerSocket ssocket = null;
	    ssocket = new ServerSocket(port);

	    //currently in use
	    int currId = 0;
	    
	    //need to put in function and wrap in thread
	    while (true) {

		//if socket accepts then go into separate thread

		Socket socket = ssocket.accept();
		PrintWriter netOutput = new PrintWriter(socket.getOutputStream());
		netOutput.println("HEADER");
		netOutput.flush();
		//getting request from the client
		Scanner sc = new Scanner(socket.getInputStream());
		    
		while (sc.hasNextLine()) {
		    //when new request is sent need to create a new thread as to handle multiple requests at once.
		    Thread t = new Thread(new clientHandle(sc.nextLine().split(" "), socket));
		    t.start();
		}
	    }
	} catch(IOException ioe) {
	    System.out.println(ioe.getMessage());
	}
    }
}

class clientHandle implements Runnable {
    private final String[] inputsList;
    private Socket socket;
    
    public clientHandle(String[] iList, Socket s) {
	inputsList = iList;
	socket = s;
    }
    
    public void run() {

	ResultSet rs;
	String PTNum, clientEmail;
	System.out.println("connected to ssocket");
	    
	try {
	    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/gym", "root", "");
	    String userInput = "";
	    System.out.println("connected");
	    DateFormat timeFormat = new SimpleDateFormat("HH:mm");
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");	
	    Statement insert = conn.createStatement();

	    //testing request against possible request types
	
	    switch (inputsList[0]) {
	    case "ADD":  
		PTNum = inputsList[1];
		clientEmail = inputsList[2];
		try {
		    //various tests for validation of input, if requent invalid send appropriate response
		    //all responses have an end to notify client response has ended
		    Date sessDate = dateFormat.parse(inputsList[3]);
		    if (!DateIsFuture(sessDate)) {
			print("Date cannot be in the past", socket);
			print("ENDLIST", socket);
			break;
		    }

		    //parsing dates into useable form
		    Date startTime = timeFormat.parse(inputsList[4]);
		    Date endTime = timeFormat.parse(inputsList[5]);

		    if (validTimes(endTime, startTime)) {
			print("ENDLIST", socket);
			break;
		    }
			
		    String focus = inputsList[6];

		    //getting response from SQL db in order to check whether client and personal trainer exist
		    Statement stat = conn.createStatement();
		    Statement stat2 = conn.createStatement();
		    ResultSet rsEmail = stat.executeQuery("SELECT * FROM Client WHERE email = " + "'" + clientEmail + "'");
		    ResultSet rsPTNum = stat2.executeQuery("SELECT * FROM PT WHERE PTNum = " + "'" + PTNum + "'");

		    if (rsEmail.next() == false) {
			print("No client associated to the client email entered", socket);
			print("ENDLIST", socket);
			break;
		    } else if (rsPTNum.next() == false) {
			print("No Personal Trainer associated with the PT-id entered", socket);
			print("ENDLIST", socket);
			break;
		    }

		    int overlapped = 0;
		    String RSCbookingQuery = "SELECT d, startTime, endTime FROM TSession WHERE clientEmail = " + "'" + clientEmail + "'";
		    Statement clientBookings = conn.createStatement();
		    ResultSet RSCBookings = clientBookings.executeQuery(RSCbookingQuery);

		    while (RSCBookings.next()) {
			if (dateEqual(RSCBookings.getDate("d"), sessDate)) {
			    if (timeClashes(startTime, endTime, RSCBookings.getTime("startTime"), RSCBookings.getTime("endTime"))) {
				overlapped = 1;
				print("Inputted Client has a booking at this time", socket);
				break;
			    }
			}
		    }

		    //getting response from SQL db in order to check whether client or personal trainer are going to have a session clash
		    String RSPbookingQuery = "SELECT d, startTime, endTime FROM TSession WHERE pNum = " + "'" + PTNum + "'";
		    Statement ptBookings = conn.createStatement();
		    ResultSet RSPBookings = clientBookings.executeQuery(RSPbookingQuery);

			    
		    while (RSPBookings.next()) {;
			if (dateEqual(RSPBookings.getDate("d"), sessDate)) {
			    if (timeClashes(startTime, endTime, RSPBookings.getTime("startTime"), RSPBookings.getTime("endTime"))) {
				overlapped = 1;
				print("Inputted Personal Trainer has a booking at this time", socket);
				break;
			    }
			}
		    }

		    if (overlapped == 1) {
			print("ENDLIST", socket);
			break;
		    }

		    //handle incorrect time or date input
		} catch(ParseException pe) {
		    print("The date or time you have inputted does not meet required format - [yyyy-MM-dd], [HH:mm]", socket);
		    print("ENDLIST", socket);
		    break;
		}
		print("Booking for PT-id: " + inputsList[1] + " with client: " + inputsList[2] + " at " + inputsList[4] + " was created successfully!", socket);
		try {
		    String newHourStart  = Integer.toString(Integer.parseInt(inputsList[4].substring(0,2)) - 1) + inputsList[4].substring(2,5);
		    String newHourEnd  = Integer.toString(Integer.parseInt(inputsList[5].substring(0,2)) - 1) + inputsList[5].substring(2,5);
		    int tryId = 0;
		    boolean foundUnique = true;
		    //go through possible values for id of booking, if available then use
		    while (foundUnique) {
			try {
			    insert.executeUpdate("INSERT INTO TSession VALUES (" +
						 tryId + ", " + "'" +
						 inputsList[1] + "'" + ", " + "'" +
						 inputsList[2] + "'" + ", " + "'" +
						 inputsList[3] + "'" + ", " + "'" +
						 newHourStart + "'" + ", " + "'" +
						 newHourEnd + "'" + ", " + "'" +
						 inputsList[6] + "'" + ")"
						 );
			    foundUnique = false;
			} catch (SQLException se) {
			    tryId++;
			}
		    }
		    print("ENDLIST", socket);
		} catch(NumberFormatException nfe) {
		    print("The date or time you have inputted does not meet required format - [yyyy-MM-dd], [HH:mm]", socket);
		    print("ENDLIST", socket);
		    break;
		}
		    
	    case "LISTALL": 
		//get response from SQL database and send to client
		rs = getRsFromQuery("SELECT * FROM TSession", conn); 
		sendResponses(rs, socket);
		print("ENDLIST", socket);
		break;
	    case "LISTPT":
		// " "
		rs = getRsFromQuery("SELECT * FROM TSession WHERE pNum = " + "'" + inputsList[1] + "'", conn);
		sendResponses(rs, socket);
		print("ENDLIST", socket);
		break;
	    case "LISTCLIENT":
		// "
		rs = getRsFromQuery("SELECT * FROM TSession WHERE clientEmail = " + "'" + inputsList[1] + "'", conn);
		sendResponses(rs, socket);
		print("ENDLIST", socket);
		break;
	    case "LISTDAY":
		try {
		    Date parsedDate = dateFormat.parse(inputsList[1]);
		    System.out.println(inputsList[1]);
		    rs = getRsFromQuery("SELECT * FROM TSession WHERE TSession.d = " + "'" + inputsList[1] + "'", conn);
		    sendResponses(rs, socket);
		    print("ENDLIST", socket);
		} catch(ParseException pe) {
		    System.out.println(pe.getMessage());
		    break;
		}
		break;
	    case "UPDATE":
		PTNum = inputsList[2];
		clientEmail = inputsList[3];
		try {
		    Date sessDate = dateFormat.parse(inputsList[4]);
		    if (!DateIsFuture(sessDate)) {
			print("Date cannot be in the past", socket);
			print("ENDLIST", socket);
			break;
		    }

		    Date startTime = timeFormat.parse(inputsList[5]);
		    Date endTime = timeFormat.parse(inputsList[6]);

		    if (endTime.getTime() < startTime.getTime()) {
			print("Start time must be before end time", socket);
			print("ENDLIST", socket);
			break;
		    } else if ((endTime.getTime() - startTime.getTime()) > 7560000) {
			print("Enter a session length between 0-180 minutes", socket);
			print("ENDLIST", socket);
			break;
		    }
			
		    String focus = inputsList[7];

		    Statement stat = conn.createStatement();
		    Statement stat2 = conn.createStatement();
		    ResultSet rsEmail = stat.executeQuery("SELECT * FROM Client WHERE email = " + "'" + clientEmail + "'");
		    ResultSet rsPTNum = stat2.executeQuery("SELECT * FROM PT WHERE PTNum = " + "'" + PTNum + "'");

		    if (rsEmail.next() == false) {
			print("No client associated to the client email entered", socket);
			print("ENDLIST", socket);
			break;
		    } else if (rsPTNum.next() == false) {
			print("No Personal Trainer associated with the PT-id entered", socket);
			print("ENDLIST", socket);
			break;
		    }

		    int overlapped = 0;
		    String RSCbookingQuery = "SELECT d, startTime, endTime FROM TSession WHERE clientEmail = " + "'" + clientEmail + "'";
		    Statement clientBookings = conn.createStatement();
		    ResultSet RSCBookings = clientBookings.executeQuery(RSCbookingQuery);

		    while (RSCBookings.next()) {
			if (dateEqual(RSCBookings.getDate("d"), sessDate)) {
			    if (timeClashes(startTime, endTime, RSCBookings.getTime("startTime"), RSCBookings.getTime("endTime"))) {
				overlapped = 1;
				print("Inputted Client has a booking at this time", socket);
				break;
			    }
			}
		    }

		    String RSPbookingQuery = "SELECT d, startTime, endTime FROM TSession WHERE pNum = " + "'" + PTNum + "'";
		    Statement ptBookings = conn.createStatement();
		    ResultSet RSPBookings = clientBookings.executeQuery(RSPbookingQuery);

			    
		    while (RSPBookings.next()) {;
			if (dateEqual(RSPBookings.getDate("d"), sessDate)) {
			    if (timeClashes(startTime, endTime, RSPBookings.getTime("startTime"), RSPBookings.getTime("endTime"))) {
				overlapped = 1;
				print("Inputted Personal Trainer has a booking at this time", socket);
				break;
			    }
			}
		    }

		    if (overlapped == 1) {
			print("ENDLIST", socket);
			break;
		    }
			
		} catch(ParseException pe) {
		    print("The date or time you have inputted does not meet required format - [yyyy-MM-dd], [HH:mm]", socket);
		    print("ENDLIST", socket);
		    break;
		}
		print("Booking for PT-id: " + inputsList[2] + " with client: " + inputsList[3] + " at " + inputsList[5] + " was updated successfully!", socket);
		try {
		    String newHourStart  = Integer.toString(Integer.parseInt(inputsList[5].substring(0,2)) - 1) + inputsList[5].substring(2,5);
		    String newHourEnd  = Integer.toString(Integer.parseInt(inputsList[6].substring(0,2)) - 1) + inputsList[6].substring(2,5);
		    insert.executeUpdate("UPDATE TSession SET " +
					 "pNum = " + "'" + inputsList[2] + "', " +
					 "clientEmail = " + "'"  + inputsList[3] + "', " +
					 "d = " + "'" + inputsList[4] + "', " +
					 "startTime = " + "'" + newHourStart + "', " +
					 "endTime = " + "'" + newHourEnd + "', " +
					 "focus = " + "'" + inputsList[7] + "'" +
					 " WHERE id = " + "'" + inputsList[1] + "'"
					 );
		    print("ENDLIST", socket);
		} catch(NumberFormatException nfe) {
		    print("The date or time you have inputted does not meet required format - [yyyy-MM-dd], [HH:mm]", socket);
		    print("ENDLIST", socket);
		    break;
		}
		break;
	    case "DELETE":
		    
		String query = "DELETE FROM TSession WHERE TSession.id = " + "'" + inputsList[1] + "'";
		Statement stat = conn.createStatement();
		int deleted = stat.executeUpdate(query);
		    
		if (deleted == 0) {
		    print("No Session with id: " + inputsList[1], socket);
		} else {
		    print("Successfully deleted Session with id: " + inputsList[1], socket);
		}
		print("ENDLIST", socket);
		break;
	    }
	} catch(SQLException se) {
	    System.out.println(se.getMessage());  
	}
    }

    public static void print(String s, Socket socket) {
	try {
	    //writing to socket, flush for immediate send
	    PrintWriter netOut = new PrintWriter(socket.getOutputStream());
	    netOut.println("1 " + s);
	    netOut.flush();
	    System.out.println(s + "flushed");
	} catch(IOException ioe) {
	    System.out.println(ioe.getMessage());
	}
    }

    public static void printList(String s, Socket socket) {
	try {
	    //" "
	    PrintWriter netOut = new PrintWriter(socket.getOutputStream());
	    netOut.println("2 " + s);
	    netOut.flush();
	    System.out.println(s + "flushed");
	} catch(IOException ioe) {
	    System.out.println(ioe.getMessage());
	}
    }

    public static void sendResponses(ResultSet rs, Socket socket) {
	try {
	    //going through resultlist sending all to socket connection
	    while (rs.next()) {
		printList("Booking ID: " + rs.getString("id") +
			  ", PT-ID: " + rs.getString("pNum") +
			  ", Client-Email: " + rs.getString("clientEmail") +
			  ", Date: " + rs.getDate("d") +
			  ", Start-time: " + rs.getTime("startTime") +
			  ", End-time: " + rs.getTime("endTime")
			  , socket);
	    }
	} catch(SQLException se) {
	    System.out.println(se.getMessage());
	}
    }

    public static ResultSet getRsFromQuery(String query, Connection con) {
	try {
	    return (con.createStatement()).executeQuery(query);
	} catch(SQLException se) {
	    System.out.println("SQL Exception : " + se.getMessage());
	}
	return null;
    }

    public static boolean DateIsFuture(Date d) {
	Date current = new Date();
	if (current.compareTo(d) > 0) {
	    return false;
	}
	return true;
    }

    public static boolean dateEqual(Date d1, Date d2) {
	if (d1.getDay() == d2.getDay() && d1.getMonth() == d2.getMonth() && d1.getYear() == d2.getYear()) {
	    return true;
	}
	return false;
    }

    public static boolean timeClashes(Date t1, Date t2, Date t3, Date t4) {
	if (t4.after(t1) && t3.before(t2)) {
	    return true;
	}
	return false;
    }

    public static boolean validTimes(Date t1, Date t2) {
	if (t1.getTime() - t2.getTime() > 0) {
	    return false;
	}
	return true;
    }
}




