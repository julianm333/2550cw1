
INSERT INTO Client VALUES
       ('bob', 'ross', 'bross@gmail.com'),
       ('jerry', 'smith', 'jsmith@outlook.com'),
       ('rick', 'sanchez', 'rsanchez@gmail.com'),
       ('morty', 'smith', 'msmith@hotmail.com');

INSERT INTO Staff VALUES
       ('beth', 'smith', 'bsmith@gmail.com', 0),
       ('barry', 'allen', 'ballen@outlook.com', 1),
       ('elon', 'musk', 'emusk@tesla.co', 2),
       ('rupak', 'lingwal', 'rlingwal@mdx.com', 3),
       ('norman', 'bates', 'nbates@yahoo.com', 4);

INSERT INTO PT VALUES
       (0, 0, 'Back'),
       (3, 1, 'Arms'),
       (4, 2, 'Biceps');

INSERT INTO TSession VALUES
       (0, 0, 'bross@gmail.com', '2020-07-07', '13:30:00', '14:30:00', 'Back');


