

DROP DATABASE gym;
CREATE DATABASE gym;
USE gym;

CREATE TABLE Client (
       fName VARCHAR(30) NOT NULL,
       lName VARCHAR(30) NOT NULL,
       email VARCHAR(40),
       PRIMARY KEY (email)
);

CREATE TABLE Staff (
       fName VARCHAR(30) NOT NULL,
       lName VARCHAR(30) NOT NULL,
       email VARCHAR(40) NOT NULL,
       staffNum VARCHAR(6),
       PRIMARY KEY (staffNum)
);

CREATE TABLE PT (
       sNum VARCHAR(6),
       PTNum VARCHAR(6),
       Speciality VARCHAR(30),
       FOREIGN KEY (sNum) REFERENCES Staff(staffNum),
       PRIMARY KEY (PTNum)
);

CREATE TABLE TSession (
       id VARCHAR(5),
       pNum VARCHAR(6),
       clientEmail VARCHAR(40),
       d DATE NOT NULL,
       startTime TIME (0) NOT NULL,
       endTime TIME (0) NOT NULL,
       focus VARCHAR(50),
       FOREIGN KEY (pNum) REFERENCES PT(PTNum),
       FOREIGN KEY (clientEmail) REFERENCES Client(email),
       PRIMARY KEY (id)
);

