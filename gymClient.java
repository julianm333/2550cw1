
//adding appropriate imports
import javafx.application.Application; 
import javafx.scene.Scene; 
import javafx.scene.control.*; 
import javafx.scene.layout.*; 
import javafx.event.ActionEvent; 
import javafx.event.EventHandler; 
import javafx.collections.*; 
import javafx.stage.Stage; 
import javafx.scene.text.Text.*;
import javafx.scene.text.*;
import javafx.scene.paint.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import java.net.*;
import java.io.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;



public class gymClient extends Application {

    public void start(Stage init) {


	final String host = "localhost";
	final int port = 4000;
	int hasSent = 0;

	try {
	    Socket socket = new Socket(host, port);

	    //adding GUI sections to the scene
	    init.setTitle("Gym Bookings");
	    TilePane tp1 = new TilePane();
	    Label l1 = new Label("this is a label test");
	    String dropOptions[] = {"Add booking", "List all bookings",
				    "Personal Trainer bookings search",
				    "Client bookings search"
				    , "Date bookings search",
				    "Update booking",
				    "Delete booking"};
	    //adding options to the combobox
	    ComboBox dropDown = new ComboBox(FXCollections.observableArrayList(dropOptions));
	    Label selected = new Label("");
	    Label k = new Label("not k");
	    Button b = new Button("b");

	    //setting up the layout
	    VBox layout = new VBox();
	    layout.setPadding(new Insets(2, 10, 10, 10));
	    layout.setSpacing(20);
	    VBox l = new VBox();
	    l.setPadding(new Insets(1, 10, 10, 10));
	    l.setSpacing(13);

	    Label temp = new Label("-- Select option from above --");
	    // making sure alignments are similar throughout the different combobox options
	    temp.setAlignment(Pos.CENTER);
	    l.getChildren().add(temp);

	    //handling event of combobox being clicked
	    EventHandler<ActionEvent> event = 
		new EventHandler<ActionEvent>() { 
		    public void handle(ActionEvent e) 
		    {
			//adding correct layout depending on what combobox option selected
			l.getChildren().clear();
			if (dropDown.getValue() == "Add booking") {
			    Label ptLabel = new Label("Personal Trainer ID");
			    TextField ptnum = new TextField("0");
			    Label cEmailLabel = new Label("Client Email");
			    TextField cEmail = new TextField("ex@e.com");
			    Label dateLabel = new Label("Booking date");
			    TextField date = new TextField("yyyy-mm-dd");
			    Label stimeLabel = new Label("Start time");
			    TextField stime = new TextField("--:--");
			    Label etimeLabel = new Label("End Time");
			    TextField etime = new TextField("--:--");
			    Label focusLabel = new Label("Focus");
			    TextField focus = new TextField("Arms");
			    Label message = new Label("");

			    Button submit = new Button("Add");
			    //once submit button is clicked, need to handle request, as well as add appropriate message to message
			    submit.setOnAction((event) ->
					       {
						   message.setText((submitInfo("ADD " +  ptnum.getText() + " " +
									      cEmail.getText() + " " +
									      date.getText() + " " +
									      stime.getText() + " " +
									      etime.getText() + " " +
									       focus.getText(), socket)).get(0));
						       }
					       );
			    //adding all the above layout sections to layout
			    l.getChildren().addAll(ptLabel, ptnum, cEmailLabel, cEmail,
						   dateLabel, date, stimeLabel, stime,
						   etimeLabel, etime, focusLabel, focus, submit, message);
		    
			} else if (dropDown.getValue() == "List all bookings") {
			    ArrayList<String> retList = submitInfo("LISTALL", socket);

			    //going through response sent from server and adding them to the layout
			    for (int i = 0; i < retList.size(); i++) {
				Label listIteration = new Label(retList.get(i));
				l.getChildren().add(listIteration);
			    }
			    
			} else if (dropDown.getValue() == "Personal Trainer bookings search") {
			    Label idLabel = new Label("Personal Trainer ID");
			    TextField id = new TextField("0");
			    Button search = new Button("Search");
			    Label message = new Label("");
			    VBox list = new VBox();
			    list.setPadding(new Insets(1, 10, 10, 10));
			    list.setSpacing(13);

			    search.setOnAction((event) -> {
				    //making sure that previous searches are cleared
				    list.getChildren().clear();
				    ArrayList<String> retList = submitInfo("LISTPT " + id.getText(), socket);
				    //doing same as above with response from server
				    if (retList.size() == 0) {
					message.setText("There are no bookings for this personal trainer!");
				    } else {
					for (int i = 0; i < retList.size(); i++) {
					    Label listIteration = new Label(retList.get(i));
					    list.getChildren().add(listIteration);
					}
				    }
				});
			    l.getChildren().addAll(idLabel, id, search, message, list);
		    
			} else if (dropDown.getValue() == "Client bookings search") {
			    //same as above
			    Label cEmailLabel = new Label("Client Email");
			    TextField email = new TextField("ex@e.com");
			    Button search = new Button("Search");
			    Label message = new Label("");
			    VBox list = new VBox();
			    list.setPadding(new Insets(1, 10, 10, 10));
			    list.setSpacing(13);

			    search.setOnAction((event) -> {
				    list.getChildren().clear();
				    ArrayList<String> retList = submitInfo("LISTCLIENT " + email.getText(), socket);
				    if (retList.size() == 0) {
					message.setText("There are no bookings for this client!");
				    } else {
					for (int i = 0; i < retList.size(); i++) {
					    Label listIteration = new Label(retList.get(i));
					    list.getChildren().add(listIteration);
					}
				    }
				});
			    l.getChildren().addAll(cEmailLabel, email, search, message, list);
		    
			} else if (dropDown.getValue() == "Date bookings search") {
			    //same as above
			    Label dateLabel = new Label("Date");
			    TextField date = new TextField("yyyy-mm-dd");
			    Button search = new Button("Search");
			    Label message = new Label("");
			    VBox list = new VBox();
			    list.setPadding(new Insets(1, 10, 10, 10));
			    list.setSpacing(13);

			    search.setOnAction((event) -> {
				    message.setText("");
				    list.getChildren().clear();
				    //checking that the date is in correct format
				    try {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date sessDate = dateFormat.parse(date.getText());
					ArrayList<String> retList = submitInfo("LISTDAY " + date.getText(), socket);
					if (retList.size() == 0) {
					    message.setText("There are no bookings on this day!");
					} else {
					    boolean printed = false;
					    for (int i = 0; i < retList.size(); i++) {
						if (!retList.get(i).equals("ENDLIST")) {
						    Label listIteration = new Label(retList.get(i));
						    list.getChildren().add(listIteration);
						    printed = true;
						}
					    }
					    if (!printed) {
						message.setText("There are no booking on this day!");
					    }
					}
				    } catch(ParseException pe) {
					message.setText("Date is not in the correct format [yyyy-MM-dd]");
				    }
				});
			    l.getChildren().addAll(dateLabel, date, search, message, list);
		    
			} else if (dropDown.getValue() == "Update booking") {
			    //same as add
			    Label idLabel = new Label("Booking ID");
			    TextField id = new TextField("0");
			    Label ptLabel = new Label("Personal Trainer ID");
			    TextField ptnum = new TextField("0");
			    Label cEmailLabel = new Label("Client Email");
			    TextField cEmail = new TextField("ex@e.com");
			    Label dateLabel = new Label("Booking date");
			    TextField date = new TextField("yyyy-mm-dd");
			    Label stimeLabel = new Label("Start time");
			    TextField stime = new TextField("--:--");
			    Label etimeLabel = new Label("End Time");
			    TextField etime = new TextField("--:--");
			    Label focusLabel = new Label("Focus");
			    TextField focus = new TextField("Arms");
			    Button submit = new Button("Update");
			    Label message = new Label("");

			    submit.setOnAction((event) -> {
				    message.setText((submitInfo("UPDATE " + id.getText() + " "
							       + ptnum.getText() + " " +
							       cEmail.getText() + " " +
							       date.getText() + " " +
							       stime.getText() + " " +
							       etime.getText() + " " +
								focus.getText(), socket)).get(0));
				}
					       );
			    l.getChildren().addAll(idLabel, id, ptLabel, ptnum, cEmailLabel, cEmail,
						   dateLabel, date, stimeLabel, stime, etimeLabel,
						   etime, focusLabel, focus, submit, message);
		    
			} else if (dropDown.getValue() == "Delete booking") {
			    Label idLabel = new Label("Booking ID");
			    TextField id = new TextField("0");
			    Button search = new Button("Delete");
			    Label message = new Label("");

			    search.setOnAction((event) -> {
				    message.setText((submitInfo("DELETE " + id.getText(), socket)).get(0));
				});
			    l.getChildren().addAll(idLabel, id, search, message);
		    
			}
		    } 
		};
	    //adding all to the scene
	    dropDown.setOnAction(event);
	    Label returnMessage = new Label();
	    layout.getChildren().add(dropDown);
	    layout.getChildren().add(l);

	    //gets the header and primes the socket for getting input
	    Scanner sc = new Scanner(socket.getInputStream());
	    if (sc.hasNextLine()) {
		String line = sc.nextLine();
	    }
	    
	    Scene scene = new Scene(layout, 800, 600);
	    init.setScene(scene);
	    init.show();
	    
	} catch(IOException ioe) {
	    System.out.println(ioe.getMessage());
	}
    }

    public static void main(String args[]) {
	launch(args);
    }

    public static ArrayList<String> submitInfo(String fieldsTogether, Socket socket) {
	ArrayList<String> ret = new ArrayList<String>();
	try {
	    //sending request to server
	    System.out.println(fieldsTogether);
	    PrintWriter netOut = new PrintWriter(socket.getOutputStream());
	    netOut.println(fieldsTogether);
	    netOut.flush();
	    //getting response from server
	    Scanner sc = new Scanner(socket.getInputStream());
	    String line = "";
	    if (sc.hasNextLine()) {
		line = sc.nextLine();
		System.out.println(line);
		ret.add(line.substring(2, line.length()));
	    }
	    //checking if server has ended the response
	    while (!line.equals("1 ENDLIST")) {
		line = sc.nextLine();
		if (!line.equals("1 ENDLIST")) {
		    ret.add(line.substring(2, line.length()));
		    System.out.println(line);
		}
	    }
	} catch(IOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	return ret;
    }
}

